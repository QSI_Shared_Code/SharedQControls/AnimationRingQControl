<h1>Description</h1>

The AnimationRing is a QControl and requires the QControl Toolkit: http://bit.ly/QControlNITools.  It inherits from and extends the Ring control, specifically a Picture Ring.  It cycles the value of the ring which contains a frame of the animation.  The frame rate is settable by property.

<h1>LabVIEW Version</h1>

This code is currently published in LabVIEW 2018.

<h1>Build Instructions</h1>

The <b>AnimationRing.lvclass</b> and all its members can be distributed as a Class Library and built into the using application.

<h1>Installation Guide</h1>

The <b>AnimationRing.lvclass</b> and all its members can be distributed as a Class Library and built into the using application.

<h1>Execution</h1>
See documentation distributed with the QControl Toolkit.

<h1>Support</h1>
Submit Issues or Merge Requests through GitLab.
